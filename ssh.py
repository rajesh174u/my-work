#!/usr/bin/env python3
# Author: rajesh Y1083
#Paramatarize version,client

import threading
import paramiko
import subprocess
import sys
import time

def ssh_fileread(ip, user, passwd, filename):
    client=paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    client.connect(ip, username=user, password=passwd)
    ssh_session=client.invoke_shell()
    time.sleep(15)
    ssh_session.send("ssh -t PSMConnect@gpam-psmp-eu.srv.allianz {} pa998138 {} -vp {}\n".format(user, ip, passwd))
    time.sleep(10)    
    ssh_session.send("adm\n")
    time.sleep(15)
    ssh_session.send("cat {}\n".format(filename))
    time.sleep(5)
    resp=ssh_session.recv(999999)
    output = resp.decode('ascii').partition(filename)[2]  
    return output
    sftp.close()
    client.close()
    

def cmd_exec(ip, user, passwd, cmd):
    client=paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    client.connect(ip, username=user, password=passwd)
    shell = client.invoke_shell()
    
    stdin, stdout, stderr = client.exec_command(cmd)
    data = stdout.read()
    
    shell.close()
    client.close()
    return data 


def asc_component_search(ip, user, passwd, component):
    cmd = ("find /var/opt/asc/ -iname {}".format(component))
    data = cmd_exec(ip, user, passwd, cmd)
    return data

def ade_component_search(ip, user, passwd, component):
    cmd = ("find /var/opt/ade/ -iname {}".format(component))
    data = cmd_exec(ip, user, passwd, cmd)
    return data  


def typefile(ip, user, passwd, component, devunit):
    cmd = ("export ADE_CFGFILE=/etc/opt/allianz/ade_{}.conf ; grep {} `asc_wrap_functions getVersionpath`/types".format(devunit,component))
    data = cmd_exec(ip, user, passwd, cmd)
    return data

#ip=input("give ip address: \n")
#user=input("give username:\n")
#passwd=input("give password: \n")
##cmd=input("[*]give commadn to execute \n")
#filename=input("give filepath: \n")
#
#ssh_fileread(ip,user,passwd,filename)