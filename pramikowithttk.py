#!/usr/local/bin/python3
import tkinter as tk
from tkinter import *
from tkinter import ttk
import SSH
 
# Root class to create the interface and define the controller function to switch frames
class RootApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.uid=StringVar()
        self.component=StringVar()
        self.cmmd=StringVar()
        self.devunit=StringVar()
        self._frame = None
        self.serverlist={
            "acs":["sla20397.srv.allianz",'indacs'],
            "cee":["sla20397.srv.allianz",'indcee'],
            "gre":["sla20399.srv.allianz",'gre'],
            "waa":["sla20238.srv.allianz",'indwaa'],
            "uwp":["sla20241.srv.allianz",'induwp'],
            "edp":["sla20243.srv.allianz",'indedp' ],
            "scb":["sla70049.srv.allianz", 'indwaa'],
            "spain":["sla70060.srv.allianz",'bga'],
            "ITMP":["sla70050.srv.allianz",'bmp']
        }
                              
        self.Login = Frame(height=100, width=20, bd=1, relief=SUNKEN)
        self.Login.pack(fill=X, padx=5, pady=5)       
        
        self.clbl = Label(self.Login, text="Client", bg="black",fg="white")
        self.clbl.pack(side=LEFT, padx=10)
        self.cli = ttk.Combobox(self.Login,width=10,values= list(self.serverlist.keys()))
        self.cli.bind('<<ComboboxSelected>>', self.serverId)
        self.cli.pack(side=LEFT)
        
        self.slbl = Label(self.Login, text="Server", bg="black",fg="white")
        self.slbl.pack(side=LEFT,padx=10)
        
        self.ser = ttk.Combobox(self.Login, text="Server")
        self.ser.pack(side=LEFT)
        
        
        self.ulbl = Label(self.Login, text="username", bg="black",fg="white")
        self.ulbl.pack(side=LEFT)
        self.usr = Entry(self.Login,width=10)
        self.usr.pack(side=LEFT)
        
        self.plbl = Label(self.Login, text="Password", bg="black",fg="white")
        self.plbl.pack(side=LEFT)
        self.passwd = Entry(self.Login,width=10, show="*")
        self.passwd.pack(side=LEFT)
  
        self.switch_frame(NoteBook)
        
    def serverId(self, event):    
        self.ser['values'] = ((self.serverlist[self.cli.get()])[0])
        self.devunit=((self.serverlist[self.cli.get()])[1])
# controller function
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()
 
# sub-root to contain the Notebook frame and a controller function to switch the tabs within the notebook
class NoteBook(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.notebook = ttk.Notebook()
        self.tab1 = Tab1(self.notebook)
        self.tab2 = Tab2(self.notebook)
        self.tab3 = Tab3(self.notebook)
        self.tab4 = Tab4(self.notebook)
        self.tab5 = Tab5(self.notebook)
        self.tab6 = Tab6(self.notebook)
        self.notebook.add(self.tab1, text="User Bash History")
        self.notebook.add(self.tab2, text="User Trace")
        self.notebook.add(self.tab3, text="asc_componentsearch")
        self.notebook.add(self.tab4, text="ade_componentsearch")
        self.notebook.add(self.tab5, text="Execute_command as YID")
        self.notebook.add(self.tab6, text="Type file check")
        self.notebook.pack(fill=BOTH, expand=Y)
 
# controller function
    def switch_tab1(self, frame_class):
        new_frame = frame_class(self.notebook)
        self.tab1.destroy()
        self.tab1 = new_frame
#################################################################################################         
# Notebook - Tab 1
class Tab1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None
        self.switch_frame(Tab1_Frame1)
        
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()
 
  
# first frame for Tab1
class Tab1_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="Userbash history")
        
        self.userid = Label(self, text ="User ID", bg="blue", fg="white")
        self.userid.pack()
        #self.newWindow = None               
        self.uidentry = Entry(self,textvariable=self.master.master.master.uid, bd=5)
        self.uidentry.bind("<Return>", lambda event: master.switch_frame(Tab1_Frame2))
        self.uidentry.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab1_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab1
class Tab1_Frame2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        self.file = "/home/{}/.bash_history_asc".format(self.master.master.master.uid.get())
        file=SSH.ssh_fileread(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.file)
        self.text.insert(END, file)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab1_Frame1))

        self.button.pack()
################################################################################################################ 
 # Notebook - User Log
class Tab2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None        
        self.switch_frame(Tab2_Frame1)       
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

# first frame for Tab2
class Tab2_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="User log")
        
        self.userid = Label(self, text ="User ID", bg="blue", fg="white")
        
        self.userid.pack()
        #self.newWindow = None               
        self.uidentry = Entry(self,textvariable=self.master.master.master.uid, bd=5)
        self.uidentry.bind("<Return>", lambda event: master.switch_frame(Tab2_Frame2))
        self.uidentry.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab2_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab2
class Tab2_Frame2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        self.file = "/home/{}/.bash_history_asc".format(self.master.master.master.uid.get())
        file=SSH.ssh_fileread(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.file)
        self.text.insert(END, file)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab2_Frame1))

        self.button.pack()
############################################################################################################################        
# Notebook - Component search in asc
class Tab3(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None        
        self.switch_frame(Tab3_Frame1)       
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

# first frame for Tab2
class Tab3_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="asc_componentsearch")
        
        self.compid = Label(self, text ="Component", bg="blue", fg="white")
        self.compid.pack()
        #self.newWindow = None               
        self.comp = Entry(self,textvariable=self.master.master.master.component, bd=5)
        self.comp.bind("<Return>", lambda event: master.switch_frame(Tab3_Frame2))
        self.comp.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab3_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab2
class Tab3_Frame2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        
        cmd=SSH.asc_component_search(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.master.master.master.component.get())
        self.text.insert(END, cmd)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab3_Frame1))

        self.button.pack()  
 

##############################################################################################################################
# Notebook - Component search in ade
class Tab4(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None        
        self.switch_frame(Tab4_Frame1)       
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

# first frame for Tab4
class Tab4_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="asc_componentsearch")
        
        self.compid = Label(self, text ="Component", bg="blue", fg="white")
        self.compid.pack()
        #self.newWindow = None               
        self.comp = Entry(self,textvariable=self.master.master.master.component, bd=5)
        self.comp.bind("<Return>", lambda event: master.switch_frame(Tab4_Frame2))
        self.comp.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab4_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab4
class Tab4_Frame2(Frame):
   
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        
        cmd=SSH.ade_component_search(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.master.master.master.component.get())
        self.text.insert(END, cmd)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab4_Frame1))

        self.button.pack()  
 

##############################################################################################################################
# Notebook - Command Execution
class Tab5(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None        
        self.switch_frame(Tab5_Frame1)       
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

# first frame for Tab5
class Tab5_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="Execute Command")
        
        self.compid = Label(self, text ="Command", bg="blue", fg="white")
        self.compid.pack()
        #self.newWindow = None               
        self.comp = Entry(self,textvariable=self.master.master.master.cmmd, bd=5, width=50)
        self.comp.bind("<Return>", lambda event: master.switch_frame(Tab5_Frame2))
        self.comp.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab5_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab5
class Tab5_Frame2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        
        cmd=SSH.ade_component_search(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.master.master.master.cmmd.get())
        self.text.insert(END, cmd)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab5_Frame1))

        self.button.pack()  
 

##############################################################################################################################
# Notebook - Command Execution
class Tab6(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._frame = None        
        self.switch_frame(Tab6_Frame1)       
        
    def switch_frame(self, frame_class):
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack(fill=BOTH, expand=Y)

# first frame for Tab5
class Tab6_Frame1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.label = Label(self, text="Componet")
        
        self.compid = Label(self, text ="Command", bg="blue", fg="white")
        self.compid.pack()
        #self.newWindow = None               
        self.comp = Entry(self,textvariable=self.master.master.master.component, bd=5)
        self.comp.bind("<Return>", lambda event: master.switch_frame(Tab6_Frame2))
        self.comp.pack()
        #self.master.master.master.myvalue="test"                    
        # button object with command to replace the frame
        self.button = Button(self, text="OK", command=lambda: master.switch_frame(Tab6_Frame2))
        self.label.pack()
        self.button.pack()     
        
# second frame for Tab5
class Tab6_Frame2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
           
        #to read the file from sever and display
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.text = Text(self, wrap=WORD, yscrollcommand=self.scrollbar.set)
        self.text.pack(expand=True, fill='both')
        self.scrollbar.config(command=self.text.yview)
        
        cmd=SSH.typefile(self.master.master.master.ser.get(),self.master.master.master.usr.get(),self.master.master.master.passwd.get(),self.master.master.master.component.get(),self.master.master.master.devunit)
        self.text.insert(END, cmd)  
        # and another button to change it back to the previous frame
        self.button = Button(self, text="back" , command=lambda: master.switch_frame(Tab6_Frame1))

        self.button.pack()  
 

##############################################################################################################################
##############################################################################################################################

 

##############################################################################################################################

if __name__ == "__main__":
    Root = RootApp()
    Root.geometry("900x900")
    Root.title("My Host APP")
    Root.mainloop()